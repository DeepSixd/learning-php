<?php
/* Поскольку какой-то мудак обрезал провода интернета в доме, я не могу работать с удаленным сервером.
Поэтому я решил порешать еще задачек. Здесь и далее будут различные простые задачи, которые
помогут мне глубже понять (и, возможно, запомнить) некоторые полезные функции PHP. Что же до работы с сервером и БД...
Попробую поработать с ними на локальном, но это совсем не то же самое. */
class VariousTasks {
	/* в этот раз я не буду указывать условие задачи - в падлу */
	public function wordRegister() {
		echo strtoupper('php') . "\r\n";
		echo strtolower('PHP') . "\r\n";
		echo ucfirst('london') . "\r\n";
		echo lcfirst('London') . "\r\n";
		echo ucwords('london is the capital of the great britain') . "\r\n";
		$string = strtolower('LONDON') . "\r\n";
		echo $newString = ucfirst($string);
	}
	public function strlenFunction() {
		echo strlen('php css html') . "\r\n";
		$password = '1052474'; //здесь нужно условие : если символов пароля >5 и <10, то пароль подходит, если нет - попросить ввести другой;
		if (strlen($password) > 10 || strlen($password) < 5) {
			echo 'Пароль должен содержать от 5 до 10 символов. Введите другой пароль.' . "\r\n";
		} else {
			echo 'Пароль подтвержден.' . "\r\n";
		}
	}
	public function substrFunction() {
		$string = 'php css html';
			echo substr($string, 0, 3);
			echo substr($string, 3, 4);
			echo substr($string, 7, 5);
			echo substr($string, -3, 3); // тут надо было вырезать 3 последних символа;
		$string = 'https://yandex.ru'; // нужно вывести "да", если начинается на хттп:// или хттпс://; если нет - "нет";
			if (substr($string, 0, 7) == 'http://' || substr($string, 0, 8) == 'https://') {
				echo "yes" . "\r\n";
			} else {
				echo "no" . "\r\n";
		}
		$string = 'Just_A_String_w/_png'; // нужно вывести "да", если заканчивается на .png или .jpg; если нет - "нет";
			if (substr($string, -4, 4) == '.png' || substr($string, -4, 4) == '.jpg') {
				echo "yes" . "\r\n";
			} else {
				echo "no" . "\r\n";
		}
		$string = 'i am learning programming and having fun'; // если в строке >5 символов - вывести 5 первых символов, а если нет - всю строку;
			if (strlen($string) > 5) {
				echo substr($string, 0, 5) . "\r\n";
			} else {
				echo $string . "\r\n";
		}
	}
	public function str_replaceFunction() {
		$string = '31.12.2013';
			echo str_replace('.', '-', $string) . "\r\n";
		$string = 'abcde';
			echo str_replace(['a', 'b', 'c'], [1, 2, 3], $string) . "\r\n";
		$string = '1523ygdsg4asar5466312fsdf';
			echo str_replace(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'], '', $string) . "\r\n"; // нужно удалить все цифры из строки;
	}
	public function strtrFunction() {
		$string = 'aaaaaaaabbbbbbbbccccccccc';
			echo strtr($string , ['a'=>'1', 'b'=>'2', 'c'=>'3']) . "\r\n"; // способ#1 - применяет массив замен;
		$string = 'aaaaaaaabbbbbbbbccccccccc';
			echo strtr($string, 'abc', '123') . "\r\n"; // способ#2 - просто указываем что на что менять;
	}
	public function substr_replaceFunction() {
		$string = '123abc456def';
			echo substr_replace($string, '!!!', 0, 5) . "\r\n";
	}
	public function strpos_strrposFunction() {
		$string = 'abc abc abc';
			echo strpos($string, 'b') . "\r\n"; // определение позиции буквы может быть произведено путем указания начала отсчета (3 параметр), но если начало не указано, то отсчет начинается с начала строки (и отсчет ведется с 0);
		$string = 'abc abc abc';
			echo strpos($string, 'b', 3) . "\r\n";
		$string = 'abc abc abc';
			echo strrpos($string, 'b') . "\r\n";
		$string = 'aaa aaa aaa aaa aaa'; // определить позицию второго пробела;
			echo strpos($string, ' ', 1) . "\r\n";
		$string = 'abcabc..abc';
			if (strpos($string, '..')) {
				echo "yes" . "\r\n";
			} else {
				echo "no" . "\r\n";
		}
		$string = 'http://yandex.ru';
			if (strpos($string, 'https://') === 0) {
				echo "yes" . "\r\n";
			} else {
				echo "no" . "\r\n";
		}
	}
	public function explode_implodeFunction() {
		$string = "html css php";
			$newArr = explode(' ', $string);
				var_dump($newArr);
		$arr = ['html', 'css', 'php'];
			$newArr = implode(', ', $arr);
				var_dump($newArr);
	}
}
$obj = new VariousTasks();

echo $obj->wordRegister();
echo $obj->strlenFunction();
echo $obj->str_replaceFunction();
echo $obj->strtrFunction();
echo $obj->substr_replaceFunction();
echo $obj->strpos_strrposFunction();
echo $obj->explode_implodeFunction();
