<?php
class Cycles
{
    //#1 Дан массив числами, например: ['10', '20', '30', '50', '235', '3000']. Выведите на экран только те числа из массива, которые начинаются на цифру 1,2 или 5.

    public function task1()
    {
        $arr = ['10', '20', '30', '50', '235', '3000'];
        foreach ($arr as $num) {
            if ($num[0] == 1) {
                echo "[ " . $num . " ]";
            }
            if ($num[0] == 2) {
                echo "[ " . $num . " ]";
            }
            if ($num[0] == 5) {
                echo "[ " . $num . " ]";
            }
        }
    }

    //#2 Дан массив с элементами 1, 2, 3, 4, 5, 6, 7, 8, 9. С помощью цикла foreach создайте строку '-1-2-3-4-5-6-7-8-9-'.

    public function task2()
    {
        $arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
        foreach ($arr as $num) {
            if ($num >= 1) {
                echo '-' . $num;
            }
            if ($num == 9) {
                echo '-';
            }
        }
    }

    //#3 Составьте массив дней недели. С помощью цикла foreach выведите все дни недели, а выходные дни выведите жирным.

    public function task3()
    {
        $week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        foreach ($week as $days) {
            if ($days == 'saturday' || $days == 'sunday') {
                echo '<b>' . $days . '</b>' . "\r\n";
            } else {
                echo $days . "\r\n";
            }
        }
    }

    //#4 Составьте массив дней недели. С помощью цикла foreach выведите все дни недели, а текущий день выведите курсивом. Текущий день должен храниться в переменной $day.

    public function task4()
    {
        $day = date('w');
        $week = ['1' => 'monday', '2' => 'tuesday', '3' => 'wednesday', '4' => 'thursday', '5' => 'friday', '6' => 'saturday', '7' => 'sunday'];
        foreach ($week as $time => $key) {
            if ($time == $day) {
                echo '<b>' . $key . '</b>' . "\r\n";
            } else {
                echo $key . "\r\n";
            }
        }
    }

    //#5 С помощью цикла for заполните массив числами от 1 до 100. То есть у вас должен получится массив [1, 2, 3... 100].

    public function task5()
    {
        $arr = array();
        for ($i = 0; $i <= 100; $i++) {
            $arr[$i] = $i;
        }
        print_r($arr); // попытка вывести echo ничего не дает, пушто 'array to string conversion'-тема;
    }

    //#6 Дан массив $arr. С помощью цикла foreach запишите английские названия в массив $en, а русские - в массив $ru.

    public function task6()
    {
        $arr = ['green' => 'зеленый', 'red' => 'красный', 'blue' => 'голубой'];
        foreach ($arr as $langEn => $langRu) {
            $en[] = $langEn;
            $ru[] = $langRu;
        }
        print_r($ru); // мне казалось, что эта задача делается как-то иначе. Ощущение, что я насрал. Федь, зови модеров, чтобы убрали :YEP:
        print_r($en);
    }
}
$obj = new Cycles();
echo $obj->task1();
echo $obj->task2();
echo $obj->task3();
echo $obj->task4();
echo $obj->task5();
echo $obj->task6();

